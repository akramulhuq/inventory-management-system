package com.super_shop.repo;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.super_shop.entity.Product;

public interface ProductRepo extends JpaRepository<Product, Long>{
	
//	@Query("SELECT i.item_brand, i.item_name, c.cate_name FROM product i INNER JOIN category c ON (c.cate_id=i.cate_id)")
//	public List<Product> findByCategory();

}
