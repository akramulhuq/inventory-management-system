package com.super_shop.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.super_shop.dto.PriceDTO;
import com.super_shop.entity.Price;
import com.super_shop.repo.PriceRepo;
import com.super_shop.repo.ProductRepo;

@Service
@Transactional
public class PriceService {

	@Autowired
	private ProductRepo productRepo;

	@Autowired
	private PriceRepo priceRepo;

	public void savePrice(PriceDTO priceDTO) {
		priceDTO.setBar_code(makePriceID());
		float retails_price = priceDTO.getRetails_price();
		float discount = priceDTO.getDiscount_rate();
		float purchase_price = (retails_price / 100 * discount);
		float totalprice = (retails_price - purchase_price);
		priceDTO.setPurchase_price(totalprice);
		priceDTO.setCreate_date(new Date());
		priceRepo.save(copyDtoToEntity(priceDTO));
	}

	public List<PriceDTO> findPrice() {
		List<Price> prices = new LinkedList<>();
		List<PriceDTO> doDtos = new LinkedList<>();
		prices = priceRepo.findAll();
		for (Price p : prices) {
			PriceDTO priceDTO = new PriceDTO();
			priceDTO.setTotal_price((float) (p.getRetails_price() * p.getItem_quantity()));
			priceDTO.setTotal_cost(p.getPurchase_price() * p.getItem_quantity());
			BeanUtils.copyProperties(p, priceDTO);
			doDtos.add(priceDTO);
		}
		// return prices.stream().map(price ->
		// copyEntityToDto(price)).collect(Collectors.toList());
		return doDtos;
	}

	public Price copyDtoToEntity(PriceDTO priceDTO) {
		Price price = new Price();
		BeanUtils.copyProperties(priceDTO, price);
		return price;
	}

	public PriceDTO copyEntityToDto(Price price) {
		PriceDTO priceDTO = new PriceDTO();
		BeanUtils.copyProperties(price, priceDTO);
		return priceDTO;
	}

	public String makePriceID() {
		long currentId = +new Date().getTime();
		String customerId = "PR11" + currentId;
		return customerId;
	}

}
