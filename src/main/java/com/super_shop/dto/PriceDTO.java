package com.super_shop.dto;

import java.util.Date;

import com.super_shop.entity.Product;

public class PriceDTO {
	
	private Long priceId;
	
	private Product product;
	
	private String productSize;
	
	private Integer retails_price;
	
	private Float purchase_price; 
	
	private Integer discount_rate;
	
	private Float total_price;
	
	private String bar_code;
	
	private Date create_date;
	
	private Integer item_quantity;
	
	private Float total_cost;

	public PriceDTO() {
		super();
	}

	public Long getPriceId() {
		return priceId;
	}

	public void setPriceId(Long priceId) {
		this.priceId = priceId;
	}

	public Product getProduct() {
		if(product==null) product=new Product();
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProductSize() {
		return productSize;
	}

	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}

	public Integer getRetails_price() {
		return retails_price;
	}

	public void setRetails_price(Integer retails_price) {
		this.retails_price = retails_price;
	}

	public Float getPurchase_price() {
		return purchase_price;
	}

	public void setPurchase_price(Float purchase_price) {
		this.purchase_price = purchase_price;
	}

	public Integer getDiscount_rate() {
		return discount_rate;
	}

	public void setDiscount_rate(Integer discount_rate) {
		this.discount_rate = discount_rate;
	}

	public String getBar_code() {
		return bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Integer getItem_quantity() {
		return item_quantity;
	}

	public void setItem_quantity(Integer item_quantity) {
		this.item_quantity = item_quantity;
	}

	public Float getTotal_price() {
		return total_price;
	}

	public void setTotal_price(Float total_price) {
		this.total_price = total_price;
	}

	public Float getTotal_cost() {
		return total_cost;
	}

	public void setTotal_cost(Float total_cost) {
		this.total_cost = total_cost;
	}
	
	

}
