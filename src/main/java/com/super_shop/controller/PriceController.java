package com.super_shop.controller;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;

import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.super_shop.dto.PriceDTO;
import com.super_shop.service.CategoryService;
import com.super_shop.service.PriceService;
import com.super_shop.service.ProductService;

@Controller
public class PriceController {
	
	@Autowired
	PriceService productPrice;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	CategoryService categoryService;

	
	@RequestMapping(path = "/price", method = RequestMethod.GET)
	public String price(Model model){
		try{
			model.addAttribute("productPriceDTO", new PriceDTO());
			model.addAttribute("prices", productPrice.findPrice());
			model.addAttribute("products", productService.findProduct());
			model.addAttribute("categorys", categoryService.findAllCate());
			
		}catch (Exception e) {
		e.printStackTrace();
		}
		return "price";
	}
	
	
	@RequestMapping(path = "/savePrice", method = RequestMethod.POST)
	public String itemSave(PriceDTO priceDTO) {
		productPrice.savePrice(priceDTO);
		return "redirect:/price";
	}
	
	

}
